///<reference types="Cypress"/>

context('Subtasks', () => {
    it('Sub', () => {
       cy.visit('https://qa-test.avenuecode.com/tasks');
  
  cy.get('.btn-primary').click();
  cy.get('input#user_email').type('vitorriosn06@gmail.com');
  cy.get('input#user_password').type('Asenhae0');
  cy.get('.btn-primary').click();
  


// Bug - 8342 (Alterar nas SubTasks para campos obrigatórios: "SubTask Description" e "DueDate")
cy.get(':nth-child(9) > :nth-child(4) > .btn').type('(3) Manage Subtasks')
cy.get('#add-subtask > .glyphicon').click();
cy.get('input#inew_sub_task').should('not.be.empty');
cy.get('input#dueDate').should('not.be.empty');

});
});

// Bug - 8341 (Ajuste de crítica ao inserir SubTask sem descrição.)
context('Sub 1', () => {
    it('Testes', () => {
cy.visit('https://qa-test.avenuecode.com/tasks');
  
  cy.get('.btn-primary').click();
  cy.get('input#user_email').type('vitorriosn06@gmail.com');
  cy.get('input#user_password').type('Asenhae0');
  cy.get('.btn-primary').click();

cy.get(':nth-child(9) > :nth-child(4) > .btn').type('(3) Manage Subtasks');
cy.get('#add-subtask > .glyphicon').click();
cy.get('[class="ng-scope ng-binding editable editable-click editable-empty"]').type ('Sub 1');
cy.get('[type="submit"]').click();

});
    });